import argparse
import json
import urllib3
import re
import sys

DESCRIPTION = """Print json from a given URL or file. Prints json from stdin
when no positional arguments are given.
"""

PRINTJSON_BAD_PATH = 1
PRINTJSON_BAD_CONTENT = 2


def _output(prefix, value, regex):
    if regex is None or regex.match(prefix):
        print(prefix + ': ' + str(value))


def _print_json(o, path, regex=None):
    prefix = ''.join(path)
    if isinstance(o, dict):
        for key, value in sorted(o.items()):
            add = '[\'{}\']'.format(key)
            if isinstance(value, dict) or isinstance(value, list):
                _print_json(value, path + [add], regex=regex)
            else:
                _output(prefix + add, value, regex)
    elif isinstance(o, list):
        for i, value in enumerate(o):
            add = '[{}]'.format(i)
            if isinstance(value, dict) or isinstance(value, list):
                _print_json(value, path + [add], regex=regex)
            else:
                _output(prefix + add, value, regex)
    else:
        _output(prefix, o, regex)


def _print_json_from_string(json_str: str, regex=None):
    try:
        o = json.loads(json_str)
    except json.JSONDecodeError as e:
        print('JSON is not valid:', e)
        return False
    _print_json(o, [], regex=regex)
    return True


def main():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('path', nargs='*')
    parser.add_argument('--regex')
    parser.add_argument(
        '--insecure', action='store_true',
        help='Ignore invalid HTTPS certificate')
    args = parser.parse_args()

    regex = None
    if args.regex is not None:
        try:
            regex = re.compile(args.regex)
        except re.error as e:
            print('Regex error:', e)
            return 1

    kwargs = {}
    if args.insecure:
        kwargs['cert_reqs'] = 'CERT_NONE'

    http = None
    ret = 0

    if len(args.path) == 0:
        json_str = sys.stdin.read()
        if not _print_json_from_string(json_str, regex=regex):
            ret = PRINTJSON_BAD_CONTENT
    else:
        for path in args.path:
            if path.startswith('http://') or path.startswith('https://'):
                if http is None:
                    http = urllib3.PoolManager(**kwargs)
                try:
                    r = http.request('GET', path)
                except urllib3.exceptions.HTTPError as e:
                    print('Unable to fetch url {} ({})'.format(path, e))
                    ret = PRINTJSON_BAD_PATH
                    continue
                json_str = r.data
            else:
                try:
                    json_str = open(path).read()
                except OSError as e:
                    print('Unable to read file {} ({})'.format(path, e))
                    ret = PRINTJSON_BAD_PATH
                    continue
            if not _print_json_from_string(json_str, regex=regex):
                ret = PRINTJSON_BAD_CONTENT

    return ret
