#!/usr/bin/env python3

from distutils.core import setup

version = '0.0.1'

setup(name='printjson',
      version=version,
      description='Print JSON objects serially from a hierarchy',
      author='Heikki Orsila',
      author_email='heikki.orsila@iki.fi',
      url='https://gitlab.com/heikkiorsila/printjson',
      py_modules=['printjson'],
      scripts=['printjson'],
      )
