#!/bin/bash

function error() {
    echo "ERROR:" "$@"
    exit 1
}

./printjson >/dev/null ./path.invalid
[[ $? != 1 ]] && error "Expected path to a non-existing file"

./printjson >/dev/null <<EOF
invalid
EOF
[[ $? != 2 ]] && error "Expected invalid json"

exit 0
