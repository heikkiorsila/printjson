# printjson

Prints json from a given URL or file.

## Example
Example: File test.json contains
```json
{"a": [{"b": 1}, {"c": 2}], "d": {"e": {"f": 3}, "g": 4}}
```

printjson test.json prints:
```
['a'][0]['b']: 1
['a'][1]['c']: 2
['d']['e']['f']: 3
['d']['g']: 4
```

## Exit codes
If the path (url or file) can not be
read, printjson returns exit code 1. If the path contains invalid json,
printjson returns exit code 2. If multiple paths are given, the last error
code (if any) is returned.

# License

Read LICENSE file.

# Authors

Read AUTHORS file.
